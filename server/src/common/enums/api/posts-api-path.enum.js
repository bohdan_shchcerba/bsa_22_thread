const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  REACT: '/react',
  REACT$ID: '/react/:postId',
  REACT_LIKED:'/react/liked',
  REACT_LIKED$ID:'/react/liked/:postId'
};

export { PostsApiPath };
