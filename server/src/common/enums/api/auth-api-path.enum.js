const AuthApiPath = {
  LOGIN: '/login',
  REGISTER: '/register',
  USER: '/user',
  USER$ID: '/user/:id',

};

export { AuthApiPath };
