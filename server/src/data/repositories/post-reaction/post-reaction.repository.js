import { Abstract } from '../abstract/abstract.repository';

class PostReaction extends Abstract {
  constructor({ postReactionModel }) {
    super(postReactionModel);
  }

  getPostReaction(userId, postId) {

    return this.model.query()
      .select()
      .where({ userId })
      .andWhere({ postId })
      .withGraphFetched('[post]')
      .first();
  }

  getLikedByUser(userId,isLike){

    return this.model.query()
      .select().where({userId}).andWhere({isLike}).withGraphFetched('[post]')
  }

  getLikesPost(postId,isLike){
    return this.model.query()
      .select().where({postId}).andWhere({isLike}).withGraphFetched('[post]')
  }


}

export { PostReaction };
