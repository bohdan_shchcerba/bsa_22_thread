import { InvalidCredentialsError } from '../../exceptions/invalid-credentials-error/invalid-credentials-error.exception';
import { ExceptionMessage } from '../../common/enums/exception/exception-message.enum';

class User {
  constructor({ userRepository }) {
    this._userRepository = userRepository;
  }

  async getUserById(id) {
    return await this._userRepository.getUserById(id);
  }

  async updateUser(id, userData) {

    if (await this._userRepository.getByUsername(userData?.username)) {
      throw new InvalidCredentialsError(
        ExceptionMessage.USERNAME_ALREADY_EXISTS
      );
    }

    return await this._userRepository.updateById(id, userData);
  }

}

export { User };
