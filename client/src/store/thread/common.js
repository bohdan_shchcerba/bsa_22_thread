const ActionType = {
  ADD_POST: 'thread/add-post',
  UPDATE_POST: 'thread/update-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  REACT: 'thread/react',
  COMMENT: 'thread/comment',
  GET_LIKED_BY_ME: 'thread/liked-by-me',
  GET_USERS: 'thead/get-users',
};

export { ActionType };

