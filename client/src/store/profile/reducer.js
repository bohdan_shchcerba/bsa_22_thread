import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import * as profileActions from './actions';

const initialState = {
  user: null,
  message: ''
};

const reducer = createReducer(initialState, builder => {
  builder
    .addCase(profileActions.updateUser.fulfilled , (state, action) => {
      state.message = ''
      state.user = action.payload;
    })

    .addMatcher(
      isAnyOf(
        profileActions.login.fulfilled,
        profileActions.logout.fulfilled,
        profileActions.register.fulfilled,
        profileActions.loadCurrentUser.fulfilled
      ),
      (state, action) => {
        state.user = action.payload;
      }
    ).addMatcher(isAnyOf(profileActions.updateUser.rejected,),(state, action) => {
      state.message = action.error.message
  })
    .addMatcher(
      isAnyOf(
        profileActions.login.rejected,
        profileActions.logout.rejected,
        profileActions.register.rejected,
        profileActions.loadCurrentUser.rejected
      ),
      state => {
        state.user = null;
      }
    );
});

export { reducer };
