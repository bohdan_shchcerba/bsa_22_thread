import {
  useState,
  useCallback,
  useEffect,
  useDispatch,
  useSelector
} from 'hooks/hooks';
import InfiniteScroll from 'react-infinite-scroll-component';
import { threadActionCreator } from 'store/actions';
import { image as imageService } from 'services/services';
import { Post, Spinner, Checkbox } from 'components/common/common';
import { ExpandedPost, SharedPostLink, AddPost } from './components/components';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10,
  inTrash: false
};

const Thread = () => {
  const dispatch = useDispatch();
  const { posts, hasMorePosts, expandedPost, userId,usersLikedPost,isLoading } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id,
    usersLikedPost: state.posts.usersLiked,
    isLoading: state.posts.isLoading,
  }));
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [showLikedByMePost, setShowLikedByMePost] = useState(false);

  const handlePostLike = useCallback(
    id => dispatch(threadActionCreator.likePost(id)),
    [dispatch]
  );

  const handlePostDislike = useCallback(
    id => dispatch(threadActionCreator.dislikePost(id)),
    [dispatch]
  );

  const handleExpandedPostToggle = useCallback(
    id => dispatch(threadActionCreator.toggleExpandedPost(id)),
    [dispatch]
  );

  const handlePostAdd = useCallback(
    postPayload => dispatch(threadActionCreator.createPost(postPayload)),
    [dispatch]
  );
  const handlePostUpdate = useCallback((postId, data) => dispatch(threadActionCreator.updatePost({ postId, data })),
    [dispatch]
  );

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };
  const handlePostsLoadLikedByMe = () => {
    dispatch(threadActionCreator.getLikedByMe());
  };
  const handleGetUsersLikedPost = (postId) => {
    dispatch(threadActionCreator.getUsersLikedPost(postId));
  };

  const handleMorePostsLoad = useCallback(
    filtersPayload => {
      dispatch(threadActionCreator.loadMorePosts(filtersPayload));
    },
    [dispatch]
  );

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowShowLikedByMe = () => {
    setShowLikedByMePost(!showLikedByMePost)
   // handlePostsLoadLikedByMe()
    if(showLikedByMePost){
      postsFilter.from = 0;
      handlePostsLoad(postsFilter);
      postsFilter.from = postsFilter.count;
    }else {
      handlePostsLoadLikedByMe()

    }


  };

  const getUsersLikedPost = (postId)=>{
    return handleGetUsersLikedPost(postId)
  }

  const getMorePosts = useCallback(() => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  }, [handleMorePostsLoad]);

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  useEffect(() => {
    getMorePosts();
  }, [getMorePosts]);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>

        <Checkbox
          isChecked={showOwnPosts}
          label='Show only my posts'
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          isChecked={showLikedByMePost}
          label='Liked by me'
          onChange={toggleShowShowLikedByMe}
        />
      </div>

      <InfiniteScroll
        dataLength={posts.length}
        next={getMorePosts}
        scrollThreshold={0.8}
        hasMore={hasMorePosts}
        loader={<Spinner key='0' />}
      >
        {posts.map(post => (
          post.inTrash ? null :
            <Post
              userId={userId}
              post={post}
              onPostLike={handlePostLike}
              onPostDislike={handlePostDislike}
              onExpandedPostToggle={handleExpandedPostToggle}
              onPostUpdate={handlePostUpdate}
              sharePost={sharePost}
              key={post.id}
              getUsersLikedPost={getUsersLikedPost}
              usersLikedPost={usersLikedPost}
              isLoading={isLoading}
            />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}

    </div>
  );
};

export default Thread;
