import { useCallback, useDispatch, useSelector } from 'hooks/hooks';
import { Image, Input } from 'components/common/common';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { ImageSize, IconName } from 'common/enums/enums';
import styles from './styles.module.scss';
import Button from '../common/button/button';
import { useState } from 'react';
import { profileActionCreator } from '../../store/actions';

const Profile = () => {
  const dispatch = useDispatch();

  const { user,message } = useSelector(state => ({
    user: state.profile.user,
    message: state.profile.message
  }));

  const [changeName,setChangeName] = useState(true)
  const [body, setBody] = useState(user.username);

  const handleUpdatePost = useCallback(
    body => dispatch(profileActionCreator.updateUser(body)),
    [dispatch]
  );


  const handleChangeName = () =>{
    setChangeName(!changeName)
    if (!changeName){
      handleUpdatePost({ 'username': body });
    }
  }
  const handleTextAreaChange = useCallback(ev => setBody(ev.target.value), [setBody]);

  return (
    <div className={styles.profile}>
      <Image
        alt="profile avatar"
        isCentered
        src={user.image?.link ?? DEFAULT_USER_AVATAR}
        size={ImageSize.MEDIUM}
        isCircular
      />
      <span>
      <Input
        iconName={IconName.USER}
        placeholder="Username"
        value={body}
        disabled={changeName}
        onChange={handleTextAreaChange}
      />
      <Button onClick={handleChangeName}>{ changeName ? 'change name' : "save" }</Button>
        {message ? <p style={{color:"red"}}> {message}</p>: null}
        </span>

      <Input
        iconName={IconName.AT}
        placeholder="Email"
        type="email"
        value={user.email}
        disabled
      />
    </div>
  );
};

export default Profile;
